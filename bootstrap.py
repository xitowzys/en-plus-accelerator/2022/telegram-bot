from dependency_injector import providers
from dependency_injector.containers import DeclarativeContainer

from core.tg_bot import ITGBot


class Container(DeclarativeContainer):
    
    conf = providers.Configuration()
    bot = providers.AbstractSingleton(ITGBot, conf.tg_bot_token)
