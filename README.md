# EN+ OSHA telegram bot

## Project structure
- requirements.txt: Python packages requirements.
- main.py: Bot entry point.
- config.yaml: Configuration parameters.
- Dockerfile: Dockerfile for docker image generation.