import logging
import dataclasses

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, PreCheckoutQueryHandler, \
    ConversationHandler, ContextTypes, CallbackQueryHandler

from infra.conversion_handler_states import *
from infra.hadlers.activate_account_handler import tg_activate_account
from infra.hadlers.menu_keyboard_handler import menu_keyboard_handler
from infra.hadlers.send_information_from_user import send_information_from_user
from infra.hadlers.question_handlers import question_keyboard_handler


@dataclasses.dataclass
class TGBot():

    def __init__(self, tg_bot_token: str, stripe_token: str = None):
        self.tg_bot_token = tg_bot_token
        self.stripe_token = stripe_token

    def register(self):
        self.updater = Updater(self.tg_bot_token, use_context=True)
        dp = self.updater.dispatcher
        dp.logger.info("Bot started")
        # Commands

        code_conversation_handler = ConversationHandler(
            entry_points=[CommandHandler('start', self.command_start, run_async=True)],
            states={
                CODE: [MessageHandler(Filters.text, tg_activate_account, run_async=True)],
                SELECTOR: [CallbackQueryHandler(menu_keyboard_handler, run_async=True)],
                QUIZ_ANSWER: [CallbackQueryHandler(question_keyboard_handler, run_async=True)],
                QUIZ: [MessageHandler(Filters.text, self.tg_message, run_async=True)],
                INFORMATION: [MessageHandler((Filters.text | Filters.photo | Filters.video), send_information_from_user, run_async=True)]
            },
            fallbacks=[CommandHandler('cancel', self.cancel)],
            run_async=True,
            allow_reentry=True

        )
        dp.add_handler(code_conversation_handler)
        #dp.add_handler(CommandHandler('start', self.tg_start, run_async=True))
        #dp.add_handler(CommandHandler('help', self.tg_quote, run_async=True))
        #dp.add_handler(CommandHandler('code', self.tg_activate_account, run_async=True))


    def start(self):
        self.updater.start_polling()

    def command_start(self, update, context):
        # print(update)
        logging.info(f"command start from {update.message.from_user.id}")
        update.message.reply_text("Enter your activation code!")
        return CODE

    def stop(self):
        self.updater.stop()

    def send_message(self, chat_id: int, text: str):
        self.updater.bot.send_message(chat_id, text)

    # /start command
    def tg_start(self, update, context):
        context.bot.send_message(chat_id=update.effective_chat.id, text="Enter activation code!")

    def tg_example(self, update, context):
        print("tg_example")
        return CODE

    # /quote command
    def tg_quote(self, update, context):
        context.bot.send_message(chat_id=update.effective_chat.id, text="tg_qoute function")

    # Message handler
    def tg_message(self, update, context):
        """
        Message handler
        """
        logging.info('Generic message handler: {}'.format(update.message.text))


    def cancel(self, update, context) -> int:
        update.message.reply_text(
            "Bye!"
        )
        return ConversationHandler.END
