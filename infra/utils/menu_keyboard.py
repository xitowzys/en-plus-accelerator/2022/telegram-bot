from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from infra import QUIZ, SCORE, INFORMATION, END, TEXTBOOK


def create_keyboard():
    keyboard = [
        [InlineKeyboardButton("Квиз", callback_data=QUIZ),
            InlineKeyboardButton("Счёт (не готово)", callback_data=SCORE)],
        [InlineKeyboardButton("Отправить данные", callback_data=INFORMATION),
        InlineKeyboardButton("Учебник (не готово)", callback_data=TEXTBOOK)],
        [InlineKeyboardButton("Закончить", callback_data=END)]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    return reply_markup
