import logging

import telegram
from telegram.ext import ConversationHandler
from infra import INFORMATION, QUIZ, QUIZ_ANSWER
from infra.hadlers.question_handlers import get_question_message


def menu_keyboard_handler(update, context):
    # ToDo add logging
    query = update.callback_query
    query.answer()
    print(query)
    if int(query.data) == INFORMATION:
        logging.info(f"Selected option: {query.data} go to Information")
        return INFORMATION
    elif int(query.data) == QUIZ:
        logging.info(f"Selected option: {query.data} go to QUIZ")
        image, text, keyboard = get_question_message()
        if image is not None:
            context.bot.sendPhoto(chat_id=query.message.chat.id, caption=text, photo=image.link, reply_markup=keyboard)

        else:
            query.message.reply_text(text=text, reply_markup=keyboard)

        return QUIZ_ANSWER
    else:
        logging.info(f"Selected option: {query.data} go to END")
        return ConversationHandler.END
