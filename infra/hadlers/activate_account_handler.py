import logging
from infra import SELECTOR, CODE
from infra.utils.menu_keyboard import create_keyboard


def tg_activate_account(update, context):
    is_active = False  # use API

    user_id = update['message'].from_user.id
    code = update['message']['text'].strip()
    print(code)
    logging.info(f"received activation code from {code} from user {user_id}")
    if code == "12345":  # TODO use API
        logging.info(f"Created start_quiz_button for {update['message'].from_user.id}")
        update.message.reply_text('Please choose:', reply_markup=create_keyboard())
        return SELECTOR
    else:
        update.message.reply_text("The code is wrong or was already activated")
        return CODE


