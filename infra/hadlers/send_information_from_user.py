import logging
from infra import INFORMATION


def send_information_from_user(self, update, context):
    if update.message.photo:
        logging.info(f"received photos: {[img.file_unique_id for img in update.message.photo]} from {update.message.from_user.id} with message {update.message.caption}")
    elif update.message.video:
        pass
        # ToDo add video handler
    elif update.message.text:
        logging.info(f"received message {update.message.text} from {update.message.from_user.id}")
    print(update)
    return INFORMATION
