import logging
import random

from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler
from app.models.QuestionModel import QuestionModel
from app.models.AnswerModel import AnswerModel
from app.models.QuestionImageModel import QuestionImageModel


def question_keyboard_handler(update, context):
    # ToDo add proper answer check via API
    # ToDo add buttons to choose either next question or quiz end
    # print(update)
    query = update.callback_query
    query.answer()
    query.edit_message_text(text=f"Selected option: {query.data}")
    received_data = query.data
    return ConversationHandler.END


def get_question_message():
    # ToDo receive question via API request
    # ToDo fix return type to telegram.Message
    question = get_test_question()

    if question is None:
        return None

    keyboard = []
    for i in range(len(question.answers)):
        keyboard.append([InlineKeyboardButton(question.answers[i].text, callback_data=question.answers[i].get_json())])

    # response = Message(text=question.text, reply_markup=keyboard)
    # return response

    return question.image, question.text, InlineKeyboardMarkup(keyboard)


def get_test_question() -> QuestionModel | None:
    question = random.randint(0, 0)
    if question == 0:
        return get_test_question_1()
    if question == 1:
        return get_test_question_2()

    return None


def get_test_question_1() -> QuestionModel:
    is_correct = [False, False, False, True]
    texts = [f"ответ {i} + {is_correct[i]}"for i in range(4)]
    question = QuestionModel(
        id=0,
        topic_id=0,
        text="Текст тестового вопроса 1",
        position=0,
        answers=[AnswerModel(i, 0, texts[i], is_correct[i]) for i in range(len(is_correct))],
        image=QuestionImageModel(0, 0, "https://picsum.photos/500")
        )

    return question


def get_test_question_2() -> QuestionModel:
    is_correct = [False, False, False, True]
    texts = [f"ответ {i} + {is_correct[i]}" for i in range(4)]
    question = QuestionModel(
        id=0,
        topic_id=0,
        text="Текст тестового вопроса 2",
        position=0,
        answers=[AnswerModel(i, 0, texts[i], is_correct[i]) for i in range(len(is_correct))],
    )

    return question
