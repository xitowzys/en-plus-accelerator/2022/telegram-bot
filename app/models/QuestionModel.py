from dataclasses import dataclass, field
from app.models.AnswerModel import AnswerModel
from app.models.QuestionImageModel import QuestionImageModel


@dataclass
class QuestionModel:
    id: int
    topic_id: int
    text: str
    position: int
    answers: list[AnswerModel] = field(default=None)
    image: QuestionImageModel = field(default=None)

# ToDo add json serialization
