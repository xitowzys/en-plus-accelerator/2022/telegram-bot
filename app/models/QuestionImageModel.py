from dataclasses import dataclass


@dataclass
class QuestionImageModel:
    id: int
    question_id: int
    link: str

