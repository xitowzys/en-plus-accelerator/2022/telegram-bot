from dataclasses import dataclass
import json


@dataclass
class AnswerModel:
    id: int
    question_id: int
    text: str
    isCorrect: int

    def get_json(self):
        return json.dumps({"id": self.id, "question_id": self.question_id})
